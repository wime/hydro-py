import pandas as pd


class System:
    """
    Hydropower system object
    """
    def __init__(self):
        self._plants = {}
        self._reservoirs = {}
        self._reservoir_plant = []
        self._plant_reservoir = []
        self._reservoir_reservoir= []

    @property
    def reservoirs(self):
        return self._reservoirs

    @reservoirs.setter
    def reservoirs(self, value):
        self._reservoirs[list(value.keys())[0]] = value[list(value.keys())[0]]

    @property
    def plants(self):
        return self._plants

    @plants.setter
    def plants(self, value):
        self._plants[list(value.keys())[0]] = value[list(value.keys())[0]]

    @property
    def reservoir_plant(self):
        return self._reservoir_plant

    @reservoir_plant.setter
    def reservoir_plant(self, value):
        self._reservoir_plant.append(value)

    @property
    def plant_reservoir(self):
        return self._plant_reservoir

    @plant_reservoir.setter
    def plant_reservoir(self, value):
        self._plant_reservoir.append(value)

    @property
    def reservoir_reservoir(self):
        return self._reservoir_reservoir

    @reservoir_reservoir.setter
    def reservoir_reservoir(self, value):
        self._reservoir_reservoir.append(value)


class Simulator(System):
    """
    Hydropower system smulator

    Attributes
    ----------
        start : datetime.datetime
            Start of simulation period
        end : datetime.datetime
            End of simulation period
        freq : string
            Frequency of simulation. Needs to be given in "H" (hourly), "D"
            (daily), etc.
    """
    def __init__(self, start, end, freq):
        super(Simulator, self).__init__()
        self.start = start
        self.end = end
        self.freq = freq
        self._set_power = {}
        self._set_local_inflow = {}
        self._set_reservoir_outflow = {}
        self.plant_values = {}
        self.reservoir_values = {}
        self.index = pd.date_range(start=start, end=end, freq=freq)[:-1]
        self._level_discharge = {}


    @property
    def set_level_discharge(self):
        return self._level_discharge

    @set_level_discharge.setter
    def set_level_discharge(self, value):
        self._level_discharge[list(value.keys())[0]] = value[list(value.keys())[0]]

    
    @property
    def set_power(self):
        return self._set_power

    @set_power.setter
    def set_power(self, value):
        self._set_power[list(value.keys())[0]] = value[list(value.keys())[0]]

    @property
    def set_local_inflow(self):
        return self._set_local_inflow

    @set_local_inflow.setter
    def set_local_inflow(self, value):
        self._set_local_inflow[list(value.keys())[0]] = value[list(value.keys())[0]]

    @property
    def set_reservoir_outflow(self):
        return self._set_reservoir_outflow

    @set_reservoir_outflow.setter
    def set_reservoir_outflow(self, value):
        self._set_reservoir_outflow[list(value.keys())[0]] = value[list(value.keys())[0]]

    @property
    def initial_head(self):
        return self._initial_head

    @initial_head.setter
    def initial_head(self, value):
        self._initial_head = value
        for res in list(value.keys()):
            self.reservoirs[res].head = value[res]

        for connection in self.reservoir_plant:
            res = connection[0]
            plant = connection[1]
            self.plants[plant].level_intake = self.reservoirs[res].head

        for connection in self.plant_reservoir:
            plant = connection[0]
            res = connection[1]
            self.plants[plant].level_discharge = self.reservoirs[res].head

    def run(self):
        for plant in self.plants:
            self.plant_values[plant] = {'power': [],
                                        'flow': []
                                       }
        for reservoir in self.reservoirs:
            self.reservoir_values[reservoir] = {'head': [],
                                                'volume': [],
                                               }
        que = [x for x in self.reservoir_reservoir]
        for i in range(len(self.index)):
            
            for plant in list(self.set_level_discharge.keys()):
                self.plants[plant].level_discharge = self.set_level_discharge[plant][i]
            
            # Set generator power
            for plant in list(self.set_power.keys()):
                self.plants[plant].power = self.set_power[plant][i]
            
            for plant in self.plant_values:
                self.plant_values[plant]['power'].append(self.plants[plant].power)
                self.plant_values[plant]['flow'].append(self.plants[plant].flow)
            for reservoir in self.reservoir_values:
                self.reservoir_values[reservoir]['head'].append(self.reservoirs[reservoir].head)
                self.reservoir_values[reservoir]['volume'].append(self.reservoirs[reservoir].volume)
                

            # Set local inflow
            for res in list(self.set_local_inflow.keys()):
                self.reservoirs[res].local_inflow = self.set_local_inflow[res][i]

            for res in list(self.set_reservoir_outflow.keys()):
                self.reservoirs[res].outflow = self.set_reservoir_outflow[res][i]

            for connection in self.reservoir_reservoir:
                res1 = connection[0]
                res2 = connection[1]
                self.reservoirs[res2].inflow = self.reservoirs[res1].outflow
                

            for connection in self.reservoir_plant:
                res = connection[0]
                plant = connection[1]
                self.reservoirs[res].outflow = self.plants[plant].flow
                self.plants[plant].level_intake = self.reservoirs[res].head

            # Calulate flow between plant and reservoir
            for connection in self.plant_reservoir:
                plant = connection[0]
                res = connection[1]
                self.reservoirs[res].inflow = self.plants[plant].flow
                self.plants[plant].level_discharge = self.reservoirs[res].head



