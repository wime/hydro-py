import sys
import os
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

here = os.path.abspath(os.path.dirname('__file__'))
README = open(os.path.join(here, 'README.md')).read()

setup(name='hydro-power-library',
      version='0.1.0',
      description='Objects to simulate a hydro power system',
      long_description=README,
      author='wime',
      author_email='willem.meijer@mailbox.org',
      url='https://gitlab.com/wime/hydro-py',
      py_modules=['hydro_power_simulator'],
      license='MIT',
      install_requires=['nested_lookup',
                        'numpy',
                       ],
     )
