import numpy as np
from nested_lookup import nested_lookup


class Constants:
    g = 9.81
    rho = 997


class Plant:
    """
    Hydropower plant object

    Attributes
    ----------
        flow_nominal : float
            Nominal waterflow through plant
        head_loss_coeff : float
            Head loss coefficient for waterway
        turbine_curve : dict
            Efficiency curve for trubine
        generator_curve : dict
            Efficiency curve for trubine
    """
    def __init__(self, flow_nominal, head_loss_coeff, turbine_curve, generator_curve):
        # Set attributes:
        self.turbine_curve = turbine_curve
        self.generator_curve = generator_curve
        self.flow_nominal = flow_nominal
        self.head_loss_coeff = head_loss_coeff
        
        self._turbine_power = None

        # Changeable Properties
        self._level_intake = None
        self._level_discharge = None

        # One of these needs to be set
        self._power = None
        self._flow = None
        
        self.power_dynamic = []
        self.flow_dynamic = []
        
        
    @property
    def turbine_efficiency(self):
        """turbine efficiency"""
        heads = np.array(list(self.turbine_curve.keys())).astype(float)
        xs2d = np.array(nested_lookup('x', self.turbine_curve))
        ys2d = np.array(nested_lookup('y', self.turbine_curve))
        xs = np.zeros(xs2d.shape[1])
        ys = np.zeros(xs2d.shape[1])
        for i in range(xs2d.shape[1]):
            xs[i] = np.interp(self.head, heads, xs2d[:, i]).round(2)
            ys[i] = np.interp(self.head, heads, ys2d[:, i]).round(2)
        efficiency = np.interp(self.flow, xs, ys)
        return efficiency / 100

    @property
    def generator_efficiency(self):
        """generator efficiency"""
        xp = self.generator_curve['x']
        yp = self.generator_curve['y']
        efficiency = np.interp(self.turbine_power, xp, yp)
        return efficiency / 100


    @property
    def turbine_power(self):
        """power at turbine"""
        return self._turbine_power
    
    @property
    def head_loss(self):
        """Head loss [m]"""
        return self.head_loss_coeff[0] * self.flow**2

    @property
    def head(self):
        """head of plant [m]"""
        return self._level_intake - self._level_discharge

    @property
    def level_intake(self):
        """level of intake [masl]"""
        return self._level_intake

    @level_intake.setter
    def level_intake(self, value):
        self._level_intake = value

    @property
    def level_discharge(self):
        """level of discharge [masl]"""
        return self._level_discharge

    @level_discharge.setter
    def level_discharge(self, value):
        self._level_discharge = value

    @property
    def power(self):
        """generator power [MW]"""
        return self._power

    @power.setter
    def power(self, value):
        self._power = value
        
        # Calcuate flow:        
        limit = 0.01
        flow = np.inf
        self._flow = self.flow_nominal
        count = 0
        while abs(self._flow - flow) > limit:
            flow = self._flow
            effective_head = self.head - self.head_loss
            self._turbine_power = self.power / self.generator_efficiency
            self._flow = round(self.turbine_power * 1e6 / (self.turbine_efficiency * effective_head * Constants.rho * Constants.g), 2)
            count += 1
            if count > 100:
                return np.nan

    @property
    def flow(self):
        """Waterflow through turbine [m3/s]"""
        return self._flow

    @flow.setter
    def flow(self, value):
        self._flow = value
        effective_head = self.head - self.head_loss
        self._turbine_power = Constants.rho * Constants.g  * self._flow * self.turbine_efficiency * effective_head * 1e-6
        self._power = round(self.turbine_power * self.generator_efficiency, 2)


class Reservoir:
    """
    Object for hydro power reservoir
    
    Attributes
    ----------
        volume_head_curve : dict
            Volume - Head curve. The format needs to be 
            {
             'x' : [volume values],
             'y' : [head values]
             }
    """
    def __init__(self, volume_head_curve):
        self.curve = volume_head_curve
        self._volume = None
        self._head = None
        self._inflow = 0
        self._outflow = 0
        self._local_inflow = 0
    
    @property
    def volume(self):
        """Volume of reservoir [mm3]"""
        return self._volume
    
    @property
    def head(self):
        """head of reservoir [masl]"""
        return self._head
    
    @head.setter
    def head(self, value):
        self._head = value
        self._volume = round(np.interp(self.head, self.curve['y'], self.curve['x']), 2)
    
    @volume.setter
    def volume(self, value):
        self._volume = value
        self._head = round(np.interp(self.volume, self.curve['x'], self.curve['y']), 2)
    
    @property
    def inflow(self):
        return self._inflow
    
    @property
    def outflow(self):
        return self._outflow
    
    @inflow.setter
    def inflow(self, value):
        self._inflow = value
        self._volume = round(self._volume + (self.inflow_total - self._outflow) * 3600 * 1e-6, 2)
        self._head = round(np.interp(self.volume, self.curve['x'], self.curve['y']), 2)
        
    @outflow.setter
    def outflow(self, value):
        self._outflow = value
        self._volume = round(self._volume + (self.inflow_total - self._outflow) * 3600 * 1e-6, 2)
        self._head = round(np.interp(self.volume, self.curve['x'], self.curve['y']), 2)

    @property
    def local_inflow(self):
        return self._local_inflow


    @local_inflow.setter
    def local_inflow(self, value):
        self._local_inflow = value
        self._volume = round(self._volume + (self.inflow_total - self._outflow) * 3600 * 1e-6, 2)
        self._head = round(np.interp(self.volume, self.curve['x'], self.curve['y']), 2)

    @property
    def inflow_total(self):
        return self.inflow + self._local_inflow

    
