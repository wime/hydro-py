from hydro_power_simulator.components import Plant, Reservoir


# Input attributes
nominal_flow = 10
head_loss_coeff = [0.001]
turbine_curve = {
        40:{'x':[8, 10, 12], 'y':[78, 85, 80]},
        60:{'x':[8, 10, 12], 'y':[84, 90, 85]},
        80:{'x':[8, 10, 12], 'y':[82, 95, 88]}
        }
generator_curve = {'x':[5, 6, 6.6],'y':[80, 95, 90]}

# Generator Plant object
plant = Plant(nominal_flow, head_loss_coeff, turbine_curve, generator_curve)

# Set level intake and discharge
plant.level_intake = 100
plant.level_discharge = 20

# Set flow
plant.flow = 9

# Print power
print(plant.power)

