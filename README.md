# HydroPy
Library containing objects to simulate a hydro power system.
`Plant` and `Reservoir` objects can be found in `components`.

## Purpose
The main purpose of the project is to develop a hydro power system simulator.
It is primarily for own use and learning. 

## Installation
The library can soon  be installed with pip:
```bash
pip install git+https://gitlab.com/wime/hydro-py.git
```

## Use
The library contains objects for reservoirs and plants and a simulaotr for a 
hydro power system. The components can be used individually or in the system 
simulaor.

### Plant
The `Plant`object has the attributes; nominal_flow, head_loss_coeff, 
turbine_curve and generator_curve.

An example:
```python
from hydro_power_simulator.components import Plant, Reservoir


# Input attributes
nominal_flow = 10
head_loss_coeff = [0.001]
turbine_curve = {
        40:{'x':[8, 10, 12], 'y':[78, 85, 80]},
        60:{'x':[8, 10, 12], 'y':[84, 90, 85]},
        80:{'x':[8, 10, 12], 'y':[82, 95, 88]}
        }
generator_curve = {'x':[5, 6, 6.6],'y':[80, 95, 90]}

# Generator Plant object
plant = Plant(nominal_flow, head_loss_coeff, turbine_curve, generator_curve)

# Set level intake and discharge
plant.level_intake = 100
plant.level_discharge = 20

# Set flow
plant.flow = 9

# Print power
print(plant.power)

```

## Roadmap
The project has the follwoing goals:
- [x] Object for power plant *- almost finished*
- [x] Object for reservoir
- [x] Simulator
- [ ] Time delays in simulator
- [ ] Simplified models where  flow between plants and reservoirs are described as GWh. 

## License
The project is published under the MIT license.
