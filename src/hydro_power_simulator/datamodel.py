from dataclasses import dataclass, field
from numbers import Number
from typing import Iterable, Optional, Dict
import numpy as np
import pandas as pd


GRAVITAIONAL_ACCELERATION = 9.81
WATER_DENSITY = 997


def resolution(time_series: pd.Series):
    return time_series.index.freq.delta.seconds


@dataclass
class LookupTable:
    """
    A lookup table with x and y values.

    Attributes
        xs {Iterable[Number]} -- An iterable with x-coords
        yx {Iterable[Number]} -- An iterable with y-coords
        x_name {Optional[str]} -- Name of x-coords
        y_name {Optional[str]} -- Name of y-coords
    """
    xs: Iterable[Number]
    ys: Iterable[Number]
    x_name: Optional[str] = None
    y_name: Optional[str] = None

    def __post_init__(self):
        if len(self.xs) != len(self.ys):
            raise AttributeError

    def __repr__(self):
        x_name = self.x_name if self.x_name else "x"
        y_name = self.y_name if self.y_name else "y"
        return f"{x_name}: {self.xs}\n{y_name}: {self.ys}"

    def __call__(
        self,
        x: Optional[Number] = None,
        y: Optional[Number] = None,
        round_value: Optional[Number] = 2,
    ) -> Number:
        if (x and y) or not (x or y):
            raise AttributeError
        elif x:
            return round(np.interp(x, self.xs, self.ys), round_value)


@dataclass
class Discharge:
    name: str

    def __init__(self):
        self._flow = None

    @property
    def water_flow(self):
        return self._water_flow

    @water_flow.setter
    def water_flow(self, value: pd.Series):
        self._water_flow = value


@dataclass
class River:
    name: str
    upstream: "Reservoir | PowerPlant | Discharge"
    downstream: "Reservoir | PowerPlant"
    time_delay: int

    def __post_init__(self):
        self._water_flow_in: pd.Series
        self._water_flow_out: pd.Series

    @property
    def water_flow_in(self):
        return self._water_flow_in

    @property
    def water_flow_out(self):
        return self._water_flow_out

    @water_flow_in.setter
    def water_flow_in(self, time_series: pd.Series):
        self._water_flow_in = time_series
        offset = int(round(self.time_delay / resolution(time_series)))
        self._water_flow_out = time_series.shift(
            periods=offset, fill_value=time_series.values[0]
        )
        self.downstream.water_flow = self._water_flow_out

    @water_flow_out.setter
    def water_flow_out(self, time_series: pd.Series):
        self._water_flow_out = time_series
        offset = int(round(self.time_delay / resolution(time_series)))
        self._water_flow_in = time_series.shift(
            periods=-offset, fill_value=self.time_series.values[-1]
        )
        self.upstream.water_flow = self._water_flow_in


@dataclass
class Reservoir:
    capacity_curve: LookupTable
    local_inflow: Discharge
    upstream: Optional["Reservoir | PowerPlant"] = None
    downstream: Optional["Reservoir | PowerPlant"] = None

    def __post_init__(self):
        self._waterlevel = None
        self._volume = None

    @property
    def waterlevel(self) -> Number:
        return self._waterlevel

    @property
    def volume(self) -> Number:
        return self._volume

    @waterlevel.setter
    def waterlevel(self, value: Number):
        self._waterlevel = value
        self._volume = self.capacity_curve(y=value)

    @volume.setter
    def volume(self, value: Number):
        self._volume = value
        self._waterlevel = self.capacity_curve(x=value)


@dataclass
class Generator:
    efficiency_curve: LookupTable
    unit: "TurbineGeneratorUnit" = field(init=False)

    def __post_init__(self):
        self._power = None
        self.efficiency = None

    @property
    def power(self):
        return self._power

    @power.setter
    def power(self, value: Number):
        # self._power = value
        self.efficiency = self.efficiency_curve(x=value, round_value=3)
        self._power = value
        limit = 0.01
        flow = np.inf
        self.unit.turbine.flow = self.unit.turbine.nominal_flow
        count = 0
        while abs(self.unit.turbine.flow - flow) > limit:
            flow = self.unit.turbine.flow
            head_loss = self.unit.power_plant.head_loss + self.unit.penstock.head_loss
            effective_head = self.unit.power_plant.head - head_loss
            self.unit.turbine.power = self._power / self.efficiency
            self.unit.turbine.flow = round(self.unit.power * 1e6 / (self.unit.turbine.efficiency * effective_head * WATER_DENSITY * GRAVITAIONAL_ACCELERATION), 2)
            count += 1
            if count > 100:
                return np.nan


@dataclass
class Turbine:
    efficiency_curve: LookupTable
    flow_nominal: Number
    unit: "TurbineGeneratorUnit" = field(init=False)

    def __post_init_(self):
        self._flow = None
        self._power = None

    @property
    def power(self):
        return self._power

    @property
    def flow(self):
        return self._flow

    @power.setter
    def power(self, value: Number):
        self._power = value
        self._flow = self.efficiency_curve(y=value, round_value=3)

    @flow.setter
    def flow(self, value: Number):
        self._flow = value
        self._power = self.efficiency_curve(x=value)
        self.unit.penstock.flow = value


@dataclass
class TurbineGeneratorUnit:
    name: str
    power_plant: "PowerPlant" = field(init=False)
    generator: Generator
    turbine: Turbine
    penstock: "Penstock"
    flow_percentage: float

    def __post_init__(self):
        self.generator.unit = self
        self.turbine.unit = self


@dataclass
class Penstock:
    head_loss_coefficient: float

    def __post_init__(self):
        self._flow = None
        self._head_loss = None

    @property
    def flow(self):
        return self._flow

    @flow.setter
    def flow(self, value: Number):
        self._value = value
        self._head_loss = self.head_loss_coefficient * value**2

    @property
    def head_loss(self):
        return self._head_loss


@dataclass
class PowerPlant:
    turbine_generator_units: Iterable[TurbineGeneratorUnit]
    upstream_reservoir: Reservoir
    downstream_reservoir: Reservoir
    penstock_common: Penstock

    def __post_init__(self):
        self._head = None
        self._head_loss
        for unit in self.turbine_generator_units:
            unit.power_plant = self

    @property
    def head(self):
        self.head = self.upstream_reservoir.waterlevel - self.downstream_reservoir.waterlevel

    @property
    def flow(self):
        return sum((unit.turbine.flow for unit in self.turbine_generator_units))

    @flow.setter
    def flow(self, value: Number):
        for unit in self.turbine_generator_units:
            unit.flow = value * unit.capacity_percentage

